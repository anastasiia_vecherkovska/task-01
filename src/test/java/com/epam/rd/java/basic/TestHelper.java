package com.epam.rd.java.basic;

import java.util.Arrays;

public class TestHelper {

	private static final boolean IS_LOG_ON = true;

	public static void logToStdErr(String expected, String actual) {
		if (IS_LOG_ON) {
			System.err.println("-----------------");
			System.err.println("expected ==> " + Arrays.toString(expected.getBytes()));
			System.err.println("actual   ==> " + Arrays.toString(actual.getBytes()));
			System.err.println("-----------------");
		}
	}

}
