package com.epam.rd.java.basic;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;

public abstract class ConsoleOutputTest {
	
	private static final InputStream STD_IN = System.in;

	private static final PrintStream STD_OUT = System.out;

	private static final String TEST_DATA_FILE_ENCODINE = "UTF-8";

	private final ByteArrayOutputStream baos = new ByteArrayOutputStream();

	private final PrintStream out = new PrintStream(baos);
	
	@BeforeEach
	void setUpControlledOut() {
		System.setOut(out);
	}

	@AfterEach void tearDownControlledOut() {
		System.setOut(STD_OUT);
	}

	@AfterEach
	void tearDownControlledIN() {
		System.setIn(STD_IN);
	}

	@ParameterizedTest
	@CsvFileSource(
		resources = "data.csv",
		numLinesToSkip = 2,
		delimiter = ';'
	)
	void shouldPrintExpectedOutput(
			@ConvertWith(LineToStringArrayConverter.class) String[] args, 
			@ConvertWith(LineToTextConverter.class) String input,
			@ConvertWith(LineToTextConverter.class) String expected) 
					throws UnsupportedEncodingException {

		byte[] buf = input.getBytes(TEST_DATA_FILE_ENCODINE);
		ByteArrayInputStream bais = new ByteArrayInputStream(buf);
		System.setIn(bais);

		print(args);

		out.flush();
		String actual = baos.toString();

		TestHelper.logToStdErr(expected, actual);

		assertEquals(expected, actual);
	}
	

	protected abstract void print(String[] args);
	
}
