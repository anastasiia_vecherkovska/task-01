package com.epam.rd.java.basic;

import org.junit.jupiter.params.converter.*;

public class LineToTextConverter extends SimpleArgumentConverter {

	private static final String EOL_CHAR = "~";
	
	@Override
	protected Object convert(Object source, Class<?> targetType) {
		if (source == null) {
			return null;
		}
		return ((String)source).replace(EOL_CHAR, System.lineSeparator());
	}

}
