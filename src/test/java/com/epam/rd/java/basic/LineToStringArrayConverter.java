package com.epam.rd.java.basic;

import org.junit.jupiter.params.converter.*;

public class LineToStringArrayConverter extends SimpleArgumentConverter {

	private static final String ARGS_SEPARATOR = "|";
	
	@Override
	protected Object convert(Object source, Class<?> targetType) {
		if (source == null) {
			return null;
		}
		return ((String)source).split('\\' + ARGS_SEPARATOR);
	}

}
